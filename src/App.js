import React from "react";
// import { CounterActions } from "./components/CounterActions";
// import { CounterContextProvider } from "./components/Context";
// import { Counter } from "./components/Counter";
import { RCounterContextProvider } from "./reducer/RContext";
import { RCounterActions } from "./reducer/RcounterActions";
import { RCounter } from "./reducer/RCounter";

function App() {
  return (
    // <CounterContextProvider>
    //   <Counter />
    //   <CounterActions />
    // </CounterContextProvider>

    <RCounterContextProvider>
      <RCounter />
      <RCounterActions />
    </RCounterContextProvider>
  );
}

export default App;
