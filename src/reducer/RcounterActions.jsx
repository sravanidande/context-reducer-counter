import * as React from "react";
import { RCounterContext } from "./RContext";

export const RCounterActions = () => {
  const { display } = React.useContext(RCounterContext);
  return (
    <div>
      <button onClick={() => display({ type: "increment" })}>+</button>
      <button onClick={() => display({ type: "decrement" })}>-</button>
      <button onClick={() => display({ type: "reset" })}>reset</button>
      <button onClick={() => display({ type: "numAdd", num: 5 })}>inc 5</button>
    </div>
  );
};
