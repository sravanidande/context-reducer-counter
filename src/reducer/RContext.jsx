import React from "react";

export const RCounterContext = React.createContext();

const CounterReducer = (state, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case "increment":
      return state + 1;
    case "decrement":
      return state - 1;
    case "reset":
      return 0;
    case "numAdd":
      return state + action.num;
    default:
      return state;
  }
};

export const RCounterContextProvider = props => {
  const [state, display] = React.useReducer(CounterReducer, 0);
  return (
    <RCounterContext.Provider value={{ state, display }}>
      {props.children}
    </RCounterContext.Provider>
  );
};
