import * as React from "react";
import { RCounterContext } from "./RContext";

// export const RCounter = () => {
//   const { state } = React.useContext(RCounterContext);
//   return <div>Count:{state}</div>;
// };

export const RCounter = () => {
  return (
    <RCounterContext.Consumer>
      {({ state }) => <div>Count:{state}</div>}
    </RCounterContext.Consumer>
  );
};
