import * as React from "react";
import { CounterContext } from "./Context";

export const Counter = () => {
  const { count } = React.useContext(CounterContext);
  return (
    <CounterContext.Consumer>
      <div>Count:{count}</div>
    </CounterContext.Consumer>
  );
};
