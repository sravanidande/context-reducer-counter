import * as React from "react";
import { CounterContext } from "./Context";

export const CounterActions = () => {
  const { increment, decrement, reset } = React.useContext(CounterContext);
  return (
    <div>
      <button onClick={increment}>+</button>
      <button onClick={decrement}>-</button>
      <button onClick={reset}>reset</button>
    </div>
  );
};
